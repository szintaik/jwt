package hu.cubifox.demo.controller;

import hu.cubifox.demo.dto.RateDTO;
import hu.cubifox.demo.exception.DemoException;
import hu.cubifox.demo.service.RateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RateController {

    private final RateService rateService;

    public RateController(RateService rateService) {
        this.rateService = rateService;
    }

    @PostMapping(value = "/rate/{productId}")
    @ExceptionHandler(DemoException.class)
    public ResponseEntity<RateDTO> saveRate(@RequestBody RateDTO rateDTO, @PathVariable(value = "productId") Long id) {
        RateDTO result=rateService.saveRate(rateDTO, id);
        return ResponseEntity.ok(result);
    }
}