package hu.cubifox.demo.controller;

import hu.cubifox.demo.dto.ProductDTO;
import hu.cubifox.demo.dto.ProductPageResponse;
import hu.cubifox.demo.exception.DemoException;
import hu.cubifox.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService productService;

    public ProductController(@Autowired ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/products")
    public ResponseEntity<ProductPageResponse> getAllProduct(@RequestParam(defaultValue = "0") int page) {
        return ResponseEntity.ok(productService.getAllProduct(page));
    }

    @GetMapping(value = "/products/{productId}")
    @ExceptionHandler(DemoException.class)
    public ResponseEntity<ProductDTO> getProduct(@PathVariable(value = "productId") Long id) {
        ProductDTO productDTO = productService.getProduct(id);
        return new ResponseEntity<>(productDTO, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping(value = "/products/{productId}")
    @ExceptionHandler(DemoException.class)
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO productDTO, @PathVariable(value = "productId") Long id) {
        productService.update(productDTO, id);
        return ResponseEntity.ok(productDTO);
    }

    @PostMapping(value = "/products")
    @ExceptionHandler(DemoException.class)
    public ResponseEntity<ProductDTO> saveProduct(@RequestBody ProductDTO productDTO) {
        productService.save(productDTO);
        return ResponseEntity.ok(productDTO);
    }
}