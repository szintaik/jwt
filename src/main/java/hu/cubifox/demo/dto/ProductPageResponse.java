package hu.cubifox.demo.dto;

import java.util.LinkedList;
import java.util.List;

public class ProductPageResponse {
    private List<ProductDTO> products = new LinkedList<>();
    private int page;

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
