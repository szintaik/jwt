package hu.cubifox.demo.dto;

import hu.cubifox.demo.model.Product;

public class RateDTO {
    private Long rateId;
    private Integer rateValue;
    private Product product;

    public Long getRateId() {
        return rateId;
    }

    public void setRateId(Long rateId) {
        this.rateId = rateId;
    }

    public Integer getRateValue() {
        return rateValue;
    }

    public void setRateValue(Integer rateValue) {
        this.rateValue = rateValue;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}