package hu.cubifox.demo.mapper;

import hu.cubifox.demo.dto.RateDTO;
import hu.cubifox.demo.model.Rate;
import org.mapstruct.Mapper;

@Mapper
public interface RateMapper {
    Rate toEntity(RateDTO rateDTO);
    RateDTO toDTO(Rate rate);
}
