package hu.cubifox.demo.mapper;

import hu.cubifox.demo.dto.ProductDTO;
import hu.cubifox.demo.model.Product;
import org.mapstruct.Mapper;

@Mapper
public interface ProductMapper {
     Product toEntity(ProductDTO productDTO);
     ProductDTO toDTO(Product product);
}
