package hu.cubifox.demo.mapper;

import hu.cubifox.demo.dto.UserDTO;
import hu.cubifox.demo.model.User;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
    User toEntity(UserDTO userDTO);
}
