package hu.cubifox.demo.service;

import hu.cubifox.demo.dto.RateDTO;
import hu.cubifox.demo.mapper.RateMapper;
import hu.cubifox.demo.model.Product;
import hu.cubifox.demo.model.Rate;
import hu.cubifox.demo.model.User;
import hu.cubifox.demo.repository.ProductRepository;
import hu.cubifox.demo.repository.RateRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import hu.cubifox.demo.exception.*;

import java.util.List;
import java.util.Optional;

@Service
public class RateService {

    private final RateRepository rateRepository;
    private final ProductRepository productRepository;
    private final RateMapper rateMapper;
    private final UserService userService;

    public RateService(RateRepository rateRepository, RateMapper rateMapper, ProductRepository productRepository, UserService userService) {
        this.rateRepository = rateRepository;
        this.rateMapper = rateMapper;
        this.productRepository = productRepository;
        this.userService = userService;
    }

    public RateDTO saveRate(RateDTO rateDTO, Long id) {
        Rate rate = rateMapper.toEntity(rateDTO);
        User actualUser = userService.findActualUser();
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent() && product.get().getUsers().contains(actualUser)) {
            rate.setProduct(product.get());
            return rateMapper.toDTO(rateRepository.save(rate));
        }
        throw new DemoException("Nem megfelelő az érték vagy nincs jogosultsága!", HttpStatus.BAD_REQUEST);
    }

    public List<Rate> getAllRate() {
        return rateRepository.findAll();
    }
}