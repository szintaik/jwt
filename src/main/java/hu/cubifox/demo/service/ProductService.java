package hu.cubifox.demo.service;

import hu.cubifox.demo.dto.ProductDTO;
import hu.cubifox.demo.dto.ProductPageResponse;
import hu.cubifox.demo.mapper.ProductMapper;
import hu.cubifox.demo.model.Product;
import hu.cubifox.demo.model.User;
import hu.cubifox.demo.repository.ProductRepository;
import hu.cubifox.demo.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import hu.cubifox.demo.exception.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private ProductMapper productMapper;
    private UserService userService;


    public ProductService(ProductRepository productRepository, ProductMapper productMapper, UserService userService) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.userService = userService;
    }

    public ProductDTO update(ProductDTO productDTO, Long id) {
        Optional<Product> actualProduct = productRepository.findById(id);
        if (actualProduct.isPresent()) {
            return productMapper.toDTO(checkUserAndUpdate(actualProduct.get(), productDTO));
        }
        throw new DemoException("Nincs ilyen id!", HttpStatus.BAD_REQUEST);
    }

    private Product checkUserAndUpdate(Product actualProduct, ProductDTO productDTO) {
        User loggedInUser = userService.findActualUser();
        Set<User> userList = actualProduct.getUsers();
        if (userList.contains(loggedInUser)) {
            actualProduct.setProductName(productDTO.getProductName());
            actualProduct.setDescription(productDTO.getDescription());
            actualProduct.setCode(productDTO.getCode());
            actualProduct.setPrice(productDTO.getPrice());
            return productRepository.save(actualProduct);
        }
        throw new DemoException("Nincsen jogosultsága!", HttpStatus.BAD_REQUEST);
    }

    public void save(ProductDTO productDTO) {
        Product product = productMapper.toEntity(productDTO);
        try {
            User user = userService.findActualUser();
            product.setUsers(user);
            productRepository.save(product);
        } catch (Exception ex) {
            throw new DemoException("Nem megfelelő az objectum", HttpStatus.BAD_REQUEST);
        }
    }

    public ProductPageResponse getAllProduct(int page) {
        int pageSize = 4;
        Page<Product> allProducts = productRepository.findAll(PageRequest.of(page, pageSize, sortByCodeAsc()));
        ProductPageResponse productPageResponse = new ProductPageResponse();
        productPageResponse.setPage(page);
        List<ProductDTO> products = productPageResponse.getProducts();

        for (Product product : allProducts) {
            products.add(productMapper.toDTO(product));
        }
        return productPageResponse;
    }

    public ProductDTO getProduct(Long productId) {
        User loggedInUser = userService.findActualUser();
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()) {
            Set<User> userList = product.get().getUsers();
            for (User item : userList) {
                if (loggedInUser.equals(item)) {
                    return productMapper.toDTO(product.get());
                }
            }
        }
        throw new DemoException("Nincs joga a megtekintéshez", HttpStatus.FORBIDDEN);
    }

    public Sort sortByCodeAsc() {
        return Sort.by("code").ascending();
    }
}