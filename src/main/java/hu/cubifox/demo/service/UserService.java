package hu.cubifox.demo.service;

import hu.cubifox.demo.dto.UserDTO;
import hu.cubifox.demo.mapper.UserMapper;
import hu.cubifox.demo.model.User;
import hu.cubifox.demo.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import hu.cubifox.demo.exception.*;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public void save(UserDTO userDTO) {
        User user=userMapper.toEntity(userDTO);
        try {
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            userRepository.save(user);
        } catch (Exception ex) {
            throw new DemoException("Username already exist!", HttpStatus.BAD_REQUEST);
        }
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public User findActualUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByUsername(auth.getName());
    }
}